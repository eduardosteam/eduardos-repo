'use strict';

/**
 * @ngdoc function
 * @name eduardoTestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the eduardoTestApp
 */
angular.module('eduardoTestApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
