'use strict';

/**
 * @ngdoc function
 * @name eduardoTestApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the eduardoTestApp
 */
angular.module('eduardoTestApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
